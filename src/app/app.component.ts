import { Component, OnInit } from '@angular/core';
import { AppServices } from './app.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  title = 'app works!';
  datas: any ;
  loading = false;
  total = 0;
  page = 1;
  limit = 20;
  constructor(private appServices: AppServices) {}

  // getData(): void {
  //   this.appServices.getJSON().then(data => this.datas = JSON.parse(data), error => console.log(error));
  // }

  ngOnInit(): void {
    this.getDatas();
  }

  getDatas(): void {
    this.loading = true;
    this.appServices.getDatas(this.page, this.limit).subscribe(res => {
      this.total = res.total;
      this.datas = res.datas;
      this.loading = false;
    });
  }

  goToPage(n: number): void {
    this.page = n;
    this.getDatas();
  }

  onNext(): void {
    this.page++;
    this.getDatas();
  }

  onPrev(): void {
    this.page--;
    this.getDatas();
  }
}
