import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent,  } from './app.component';
import { PrettyPrintPipe,  } from './app.pipe';

import { AppServices } from './app.service';
import { PaginationComponent } from './pagination/pagination.component';
import { AccordionComponent } from './accordion/accordion.component';
import { AccordionGroupComponent } from './accordion/accordion-group/accordion-group.component';

@NgModule({
  declarations: [
    AppComponent,
    PrettyPrintPipe,
    PaginationComponent,
    AccordionComponent,
    AccordionGroupComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [AppServices],
  bootstrap: [AppComponent]
})
export class AppModule { }
