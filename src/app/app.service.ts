import { Component, Input } from '@angular/core';
import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { data } from './test.data';
@Injectable()
export class AppServices {

    constructor(private http: Http) {
    }

    getDatas(page: number, limit: number) {
        const temp = {
            total: JSON.parse(data).length,
            datas: JSON.parse(data).slice((page - 1 ) * limit, page * limit )
        };
        return Observable.of(temp);
    }
}
