import { Component, OnDestroy, Input, OnChanges, SimpleChange } from '@angular/core';
import { AccordionComponent } from '../accordion.component';

@Component({
  selector: 'accordion-group',
  templateUrl: './accordion-group.component.html',
  styleUrls: ['./accordion-group.component.scss'],
})
export class AccordionGroupComponent implements OnDestroy, OnChanges {
    @Input() heading: string;
    @Input() isOpen: boolean;
    @Input() index: number;

    constructor(private accordion: AccordionComponent) {
        this.accordion.addGroup(this);
    }

    ngOnDestroy() {
        this.accordion.removeGroup(this);
    }

    ngOnChanges(changes: {[propKey: string]: SimpleChange}){
        for (const propName in changes){
            if (changes.hasOwnProperty(propName)) {
                const changedPro = changes[propName];
                if(!changedPro.isFirstChange()){
                    this.accordion.groups[this.index + 1].toggleOpen();
                }
            }
        }
    }

    toggleOpen(): void {
        if (!this.isOpen) {
            this.isOpen = true;
            this.accordion.closeOthers(this);
        } else {
            this.isOpen = false;
        }
    }
}
